import { defineStore } from "pinia";
import { db } from "boot/firebase"; // no olvidar importar db

export const useTodoStore = defineStore("todoStore", {
  state: () => ({
    todoList: [],
    archivedList: [],
    newTodo: {},
    count: 0,
  }),

  getters: {
    async getTodoList(state) {
      let tasks = [];

      try {
        const query = await db.collection('notes').get();
        console.log(db)
        console.log(query)
        query.forEach(element => {
          var finish = new Date(element.data().finishBy.seconds * 1000 + element.data().finishBy.nanoseconds/1000000)
          let task = {id: element.id, todo: element.data().todo, done: element.data().done, finishBy: finish}
          tasks.push(task);
        });
      } catch (error) {
        console.log(error);
      } 
      console.log(tasks)
      return tasks;
    },
    getCount(state) {
      return state.count;
    },
    getArchivedList(state) {
      return state.archivedList;
    },
  },

  actions: {
    addTodo(todo) {
      this.todoList.push(todo);
      console.log(this.todoList);
    },
    editTodo(editTodo, todo, finishBy) {
      let index = this.todoList.findIndex((el) => el == editTodo);
      this.todoList[index].todo = todo;
      this.todoList[index].finishBy = finishBy;
    },
    deleteTodo(id) {
      let index = this.todoList.findIndex((el) => el.id == id);
      this.todoList.splice(index, 1);
    },
    archiveTodo(id) {
      let index = this.todoList.findIndex((el) => el.id == id);
      let spliced = this.todoList.splice(index, 1);
      spliced[0].archivedOn = Date.now();
      this.archivedList.push(spliced[0]);
    },
    addCount() {
      this.count++;
    },
  },
});
