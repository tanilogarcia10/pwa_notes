
// import * as firebase from "firebase/app";
// import "firebase/firestore";
// v9 compat packages are API compatible with v8 code
import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';

// Agregar configuración firebase:
var firebaseConfig = {
    apiKey: "AIzaSyDuHKW87L1u39WoDxBKyAd2Jhh8YbrLvtc",
    authDomain: "mynotes-ca6b0.firebaseapp.com",
    projectId: "mynotes-ca6b0",
    storageBucket: "mynotes-ca6b0.appspot.com",
    messagingSenderId: "150159774112",
    appId: "1:150159774112:web:85b4df38a34d82805354c5",
    measurementId: "G-TMWQTEW5CT"
};

let firebaseApp = firebase.initializeApp(firebaseConfig);
let db = firebase.firestore();

export { db, firebase };

