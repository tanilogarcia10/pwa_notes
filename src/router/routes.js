const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      { path: "todo", component: () => import("pages/TodoNewEdit.vue") },
      { path: "todo/:id", component: () => import("pages/TodoNewEdit.vue") },
      { path: "archived", component: () => import("pages/ArchivedPage.vue") },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
